import Cell from '../src/cell'

describe('cell', () => {
    it('could be alive', () => {
        // Arrange
        const alive = true
        const cell = new Cell(alive)

        // Act
        const result = cell.isAlive()

        // Assert
        expect(result).toBe(true)
    })

    it('could be dead', () => {
        const alive = false
        const cell = new Cell(alive)

        const result = cell.isAlive()

        expect(result).toBe(false)
    })

    it('should know her position', () => {
        const alive = false
        const position = [0,0]
        const cell = new Cell(alive, position)

        const result = cell.whereIs()

        expect(result).toEqual([0,0])
    })

    it('should knows her alive neigthbors', () => {
      const cell1 = new Cell(true, [0,0])
      const cell2 = new Cell(true, [0, 1])
      const cell3 = new Cell(true, [0, 2])
      const cell4 = new Cell(true, [0, 3])

      const neigthborsList = [cell1, cell2, cell3, cell4]

      const result = cell2.howManyNeigthborsAlive(neigthborsList)

      expect(result).toBe(2)
    })
})

export default class Cell {
  constructor(alive = true, position = [0,0]) {
    this.alive = alive
    this.position = position
  }

  isAlive() {
    return this.alive
  }

  whereIs() {
    return this.position
  }

  howManyNeigthborsAlive(cellList) {
    return cellList
        .filter(cell => cell.whereIs() != this.position)
        .filter(cell => Math.abs(cell.whereIs()[1] - this.position[1]) === 1)
        .reduce((aliveNeigthbors, cell)=>{
          if(cell.isAlive()){
            return aliveNeigthbors += 1
          }
        }, 0)
  }
}
